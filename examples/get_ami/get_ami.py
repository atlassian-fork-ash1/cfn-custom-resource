"""
get_ami.py - Get AMI ID by name, owner and tags.

Input:
* name - Image name
* owner - Image owner
* latest - if True returns latest
* tags - Tags dict

Output:
* ImageId - Image id
"""

from collections import namedtuple

from cfn_custom_resource import lambda_handler, create, update, delete, ResourceError

import boto3
from botocore.exceptions import ClientError


@create()
def create(event, _):
    data = {}
    props = get_properties(event)

    filters = [
        {'Name': 'name', 'Values': [props.name]},
        {'Name': 'state', 'Values': ['available']},
        *[{'Name': 'tag:'+k, 'Values': [v]} for k, v in props.tags.items()]
    ]

    ec2 = boto3.client('ec2')
    try:
        r = ec2.describe_images(
            Owners=[props.owner],
            Filters=filters
        )
    except ClientError as e:
        raise ResourceError(f'failed describing images: {e}')

    images = r['Images']
    if len(images) == 0:
        raise ResourceError('No image found')
    elif len(images) == 1 or not props.latest:
        data.update(ImageId=images[0]['ImageId'])
    else:
        latest = sorted(images, key=lambda x: x['CreationDate'], reverse=True)[0]['ImageId']
        data.update(ImageId=latest)

    return None, data


@update()
def update(event, context):
    # update and delete does the same
    return create(event, context)


@delete()
def delete(event, context):
    # delete doesn't do anything
    return


def get_properties(event):
    props = event['ResourceProperties']
    out = namedtuple('Properties', 'name owner latest tags')
    try:
        o = out(name=props['Name'],
                owner=props['Owner'],
                latest=props.get('Latest', True),
                tags=props.get('Tags', {}))
    except KeyError as e:
        raise ResourceError(f'Missing property: {str(e)}')

    return o
