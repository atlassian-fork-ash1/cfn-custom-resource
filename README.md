# cfn-custom-resource

Python helper library for creation of Lambda based [Custom Resource](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/template-custom-resources.html) 
for AWS CloudFormation. The library allows the author to focus on the logic of creation/update/removal of the Custom 
Resource instead of the details of the request/response format that CloudFormation expects. 


# Installation

To install simply use pip:

    pip install cfn-custom-resource
    

# Basic usage

Here's how a basic Custom Resource can be created:

    from cfn_custom_resource import lambda_handler, create, update, delete
	
	@create()
	def create(event, context):
	  # .. provision resource ..
	  
	  # resource_id = ...
	  # output_attribures = {} 
	  
	  return resource_id, output_attributes
	  
	@update()
	def update(event, context):
	  # .. update resource ..
	  # if resource_id changed cloudformation would trigger delete 
	  
	  return resource_id, output_attributes
	  
	@delete()
	def delete(event, context):
	  # .. cleanup ..
	  
	  return
	  
Check [the wiki](https://bitbucket.org/atlassian/cfn-custom-resource/wiki/Home) for more details.	  


# Contributing

See [CONTRIBUTING.md](https://bitbucket.org/atlassian/cfn-custom-resource/src/master/CONTRIBUTING.md)


# License

Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](https://bitbucket.org/atlassian/cfn-custom-resource/src/master/LICENSE) file.
